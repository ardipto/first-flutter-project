import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'color_code.dart';

// stateless widget & stateful widget

void main(){
  runApp(MaterialApp(
    title: 'My First App',
    home: HomePage()
  ));
}

class HomePage extends StatelessWidget{
  final _longText = "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter"
      "in the 15th century who is thought to have scrambled parts of Cicero's De Finibus Bonorum et Malorum for use in a type specimen book.";
  final _shortText = "Lorem ipsum, or lipsum as it is sometimes known";
  List people = [
    {"name": "Dipto", "email": "ardipta82@gmail.com"},
    {"name": "sfdg", "email": "xdfg@gmail.com"},
    {"name": "cvvc", "email": "jhg@gmail.com"},
    {"name": "Dipsreto", "email": "sfdg@gmail.com"},
    {"name": "Ddcgvipto", "email": "sfdg@gmail.com"},
    {"name": "fdgf", "email": "sfdgc@gmail.com"},
    {"name": "cxv", "email": "sdfg@gmail.com"},
    {"name": "fdsger", "email": "ffdfg@gmail.com"},
    {"name": "grsdg", "email": "adfg@gmail.com"},
    {"name": "fsdg", "email": "cxh@gmail.com"},
    {"name": "sdfgv", "email": "hjjcfg@gmail.com"},
    {"name": "gfhfg", "email": "ardgrdgipta82@gmail.com"},
    {"name": "vdcfgfd", "email": "fdg@gmail.com"},
    {"name": "zcxg", "email": "ardipsdfgta82@gmail.com"},
    {"name": "wad", "email": "sdfg@gmail.com"},
  ];
  @override
  Widget build(BuildContext context){
    return Scaffold(
        appBar: AppBar(
            title: Text('Home'),
            centerTitle: true,
        ),
        drawer: Drawer(
          child: ListView(
            children: [
              Stack(
                children: [
                  Image.asset("assets/images/background.jpg"),
                  Positioned(
                    left: 30,
                    bottom: 50,
                    child: Container(
                      height: 50,
                      width: 50,
                      child: Image.asset("assets/images/profile.png"),
                    ),
                  ),
                  Positioned(
                    left: 30,
                    bottom: 20,
                    child: Text("Hi, John Smith", style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.bold),),
                  )
                ],
              ),
              SizedBox(height: 30,),
              ListTile(
                leading: Icon(Icons.email),
                title: Text("Email"),
                subtitle: Text("john.smith@gmail.com"),
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.phone),
                title: Text("Phone"),
                subtitle: Text("+8801797174800"),
              ),
              Divider(),
              ListTile(
                leading: Icon(Icons.add_location),
                title: Text("Address"),
                subtitle: Text("Dhaka-1212"),
              ),
              Divider(),


            ],
          ),
        ),
        body: ListView( // make screen content scrollable
          children: [
            Column(
              children: [
                Image.asset("assets/images/mockup.png"),
                Container(
                  child: Text(_longText+_longText, textAlign: TextAlign.justify,),
                  padding: EdgeInsets.all(20),
                ),
                Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(left: 20, top: 20),
                      padding: EdgeInsets.all(5),
                      height: 40,
                      width: 50,
                      decoration: BoxDecoration( // text & color setting of a container
                          color: Color(ColorCode.getHaxToInt("#F7CD2E")),
                          borderRadius: BorderRadius.circular(8)),
                      child: Text('Hello Flutter', style: TextStyle(color: Colors.black, fontSize: 12, fontWeight: FontWeight.bold),),
                    ),
                    Container(
                        margin: EdgeInsets.only(left: 20, top: 20),
                        padding: EdgeInsets.all(5),
                        height: 40,
                        width: 50,
                        foregroundDecoration: BoxDecoration( // image set in a container
                            image: DecorationImage(
                              image: NetworkImage("https://imgsv.imaging.nikon.com/lineup/dslr/df/img/sample/img_01.jpg"),
                            )
                        ),
                        decoration: BoxDecoration( // text & color setting of a container
                            color: Color(ColorCode.getHaxToInt("#F7CD2E")))
                      // child: Text('Hello Flutter', style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, top: 20),
                      padding: EdgeInsets.all(5),
                      height: 40,
                      width: 50,
                      decoration: BoxDecoration( // text & color setting of a container
                          color: Color(ColorCode.getHaxToInt("#F7CD2E")),
                          borderRadius: BorderRadius.circular(8)),
                      child: Icon(Icons.add_a_photo_rounded), // setting icon inside container
                    ),
                  ],
                ),
                Stack( // image with test positioning
                  children: [
                    Image.asset("assets/images/pic.jpg"),
                    Positioned(
                      bottom: 30,
                      left: 40,
                        child: Text('Hello Flutter', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20))
                    )
                  ],
                ),
                SizedBox( // like padding or br or white space
                  height: 20,
                ),
                Text('Feature Text', style: TextStyle(color: Colors.black87, fontWeight: FontWeight.bold, fontSize: 20)),
                SizedBox( // like padding or br or white space
                  height: 20,
                ),
                // SizedBox( // sizebox with container inside
                //   height: 50,
                //   width: 100,
                //   child: Container(
                //     decoration: BoxDecoration(
                //       color: Colors.red
                //     ),
                //   ),
                // ),
                Row(
                  children: [
                    _getContainer("#673ab7"),
                    _getContainer("#3f51b5"),
                    _getContainer("#ffc107"),
                    _getContainer("#ff5722"),
                    _getContainer("#795548"),
                  ],
                ),
              ],
            ),
            Column( // scroll inside container
              children: [
                Container(
                  height: 220,
                  padding: EdgeInsets.all(20),
                  child: ListView(
                    // scrollDirection: Axis.horizontal, // scroll horizontally
                    children: [
                      _getListTile("MD. Ashiqur Rahaman", "ardipta82@gmail.com"),
                      Divider(),
                      _getListTile("sxffs asdfdf", "adsfdsa@gmail.com"),
                      Divider(),
                      _getListTile("asdfsasd dasfadsf", "adsfasf@gmail.com"),
                      Divider(),
                      _getListTile("fwdfs fads", "dtrfdf@gmail.com"),
                      Divider(),
                      _getListTile("arefdf asffg", "trsdfgsd@gmail.com"),
                      Divider(),
                      // _cell(),
                      // Divider(),
                      // _cell(),
                      // Divider(),
                      // _cell(),
                      // Divider(),
                      // _cell(),
                      // Divider(),
                      // _cell(),
                    ],
                  ),
                )
              ],
            ),
            Column(
              children: [
                Container(
                  height: 220,
                  padding: EdgeInsets.all(10),
                  child: ListView.builder(
                    itemCount: people.length,
                    itemBuilder: (BuildContext context, int index){
                      return Column(
                        children: [
                          ListTile(
                            leading: CircleAvatar(
                              child: Text(people[index]["name"][0]),
                            ),
                            title: Text(people[index]["name"]),
                            subtitle: Text(people[index]["email"]),
                          )
                        ],
                      );
                    },
                  ),
                )
              ],
            ),
            Column(
              children: [
                getFoodCard("assets/images/pizza.jpg", "Pizza", "Price \$22", "222"),
                Divider(height: 10),
                getFoodCard("assets/images/burger.jpg", "Burger", "Price \$25", "251"),
                Divider(height: 10),
                getFoodCard("assets/images/fried.jpg", "Fried Chicken", "Price \$30", "280"),
              ],
            )
          ],
        )
      );
  }
}

Widget _getContainer(String _color){ // get same functions how many times I want
  return Expanded( // gap maintain automatically like col-lg, md etc
      child: Container(
        height: 40,
        width: 60,
        decoration: BoxDecoration(
            color: Color(ColorCode.getHaxToInt(_color))
        ),
        child: Icon(Icons.account_balance,color: Colors.white)
      )
  );

}

Widget _cell(){
  return Row(
    children: [
      Container(
        width: 100,
        height: 100,
        decoration: BoxDecoration(
          color: Colors.amberAccent
        ),
        child: Icon(Icons.access_alarm_outlined, color: Colors.white,),
      ),
      SizedBox(
        width: 10,
      )
    ],
  );
}

Widget _getListTile(String name, String email){
  return ListTile(
    leading: CircleAvatar( // circle inside anything
      child: Icon(Icons.account_circle),
    ),
    title: Text(name),
    subtitle: Text(email),
    onTap: (){

    },
  );
}

Widget getFoodCard(String imgSrc, String title, String price, String salesTime){
  return Material(
    elevation: 10,
    child: Card(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Image.asset(imgSrc),
          Padding(
            padding: EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(title, style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
                SizedBox(height: 5,),
                Text(price, style: TextStyle(fontSize: 14)),
                SizedBox(height: 5,),
                Text("Sale $salesTime Times", style: TextStyle(fontSize: 14)),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}