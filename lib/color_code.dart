class ColorCode{
  static int getHaxToInt(String haxCode){
    haxCode = "FF" + haxCode;
    haxCode = haxCode.replaceAll("#", "");
    int val = 0;
    int len = haxCode.length;
    for(int i = 0; i<len; i++){
      int haxDigit = haxCode.codeUnitAt(i);
      if (haxDigit >= 48 && haxDigit <=57) {
        val +=(haxDigit - 48) * (1 << (4 * (len-1-i)));
      }  
      else if (haxDigit>=65 && haxDigit <=70) {
        val +=(haxDigit - 55) * (1 << (4 * (len-1-i)));
      }
      else if (haxDigit>=97 && haxDigit <=102) {
        val +=(haxDigit - 87) * (1 << (4 * (len-1-i)));
      }
      else{
        throw FormatException("An error occur");
      }
    }
    return val;
  }
}